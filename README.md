# Tetris project

This is the public repository for project Tetris.

## Software requirements

* Xilinx Vivado Design Suite 2019.2 (with Vitis);
* Download the [ZCU104 System Controller](https://www.xilinx.com/support/documentation-navigation/design-hubs/dh0036-zynq-ultrascale-plus-mpsoc-hub.html) to set the FMC Vadj voltage to 1.8 V;

## Installation procedure

You do not need to open Vivado to run this project on the platform, Vitis alone is necessary.

### Setting up the Vitis project
Steps to import the project in Vitis:
* Clone the project from gitlab;
* Open Vitis and select a suitable workspace;
* Go to **File -> Import -> Vitis project exported zip file** and select the zip file in the *vitis* folder, click **Finish** to import it;
* Right click anywhere in the *explorer* window, select **New -> Platform Project**;
* Name the project **Tetris_pf** and click **Next**;
* Select **Create from hardware specification (XSA)**;
* Select the **.xsa** file provided in the *vitis* folder, leave the software specification as is and click **Finish**;

### Building the Vitis project
Steps to build the project:
* In the *explorer* tab, right-click on your platform name and select **Build project**. This may take several minutes;
* Once the platform is built, the project has to be built as well. In the *explorer* tab, right-click on *Tetris_system* and select **Build project**;

### Setting up the Vivado project (optional)
Steps to import the project into Vivado:
To come.

## Starting the project
* First make sure that the *SW6 MODE SWITCH* DIP switches are all set to ON to boot from JTAG;
* Plug in the camera sensor, the HDMI cable, the micro USB cable and the power cable. Switch the board to ON;
* Open up the ZCU104 System Controller and select the board;
	* Go to the *About* tab and select **Program System Controller**. Let it finish it should take a few seconds;
	* Go to the *FMC* tab and select **Set VADJ to 1.8V**. LED DS8 should not be lit on the board;
* In Vitis, right-click the *Tetris_system* project and select **Run As -> Launch on Hardware (System Project Debug)**;
* The *Debug* perspective of Vitis should open and program the FPGA. In the tabs at the bottom, open *Vitis Serial Terminal* and open a terminal with the device;
* If you missed the bootloader information, you can type in 'i' to get menu information;