// ==============================================================
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2019.2_AR73100 (64-bit)
// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// ==============================================================
/***************************** Include Files *********************************/
#include "xvideo_proc.h"

/************************** Function Implementation *************************/
#ifndef __linux__
int XVideo_proc_CfgInitialize(XVideo_proc *InstancePtr, XVideo_proc_Config *ConfigPtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(ConfigPtr != NULL);

    InstancePtr->Bus_a_BaseAddress = ConfigPtr->Bus_a_BaseAddress;
    InstancePtr->IsReady = XIL_COMPONENT_IS_READY;

    return XST_SUCCESS;
}
#endif

void XVideo_proc_Start(XVideo_proc *InstancePtr) {
    u32 Data;

    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XVideo_proc_ReadReg(InstancePtr->Bus_a_BaseAddress, XVIDEO_PROC_BUS_A_ADDR_AP_CTRL) & 0x80;
    XVideo_proc_WriteReg(InstancePtr->Bus_a_BaseAddress, XVIDEO_PROC_BUS_A_ADDR_AP_CTRL, Data | 0x01);
}

u32 XVideo_proc_IsDone(XVideo_proc *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XVideo_proc_ReadReg(InstancePtr->Bus_a_BaseAddress, XVIDEO_PROC_BUS_A_ADDR_AP_CTRL);
    return (Data >> 1) & 0x1;
}

u32 XVideo_proc_IsIdle(XVideo_proc *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XVideo_proc_ReadReg(InstancePtr->Bus_a_BaseAddress, XVIDEO_PROC_BUS_A_ADDR_AP_CTRL);
    return (Data >> 2) & 0x1;
}

u32 XVideo_proc_IsReady(XVideo_proc *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XVideo_proc_ReadReg(InstancePtr->Bus_a_BaseAddress, XVIDEO_PROC_BUS_A_ADDR_AP_CTRL);
    // check ap_start to see if the pcore is ready for next input
    return !(Data & 0x1);
}

void XVideo_proc_EnableAutoRestart(XVideo_proc *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XVideo_proc_WriteReg(InstancePtr->Bus_a_BaseAddress, XVIDEO_PROC_BUS_A_ADDR_AP_CTRL, 0x80);
}

void XVideo_proc_DisableAutoRestart(XVideo_proc *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XVideo_proc_WriteReg(InstancePtr->Bus_a_BaseAddress, XVIDEO_PROC_BUS_A_ADDR_AP_CTRL, 0);
}

void XVideo_proc_InterruptGlobalEnable(XVideo_proc *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XVideo_proc_WriteReg(InstancePtr->Bus_a_BaseAddress, XVIDEO_PROC_BUS_A_ADDR_GIE, 1);
}

void XVideo_proc_InterruptGlobalDisable(XVideo_proc *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XVideo_proc_WriteReg(InstancePtr->Bus_a_BaseAddress, XVIDEO_PROC_BUS_A_ADDR_GIE, 0);
}

void XVideo_proc_InterruptEnable(XVideo_proc *InstancePtr, u32 Mask) {
    u32 Register;

    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Register =  XVideo_proc_ReadReg(InstancePtr->Bus_a_BaseAddress, XVIDEO_PROC_BUS_A_ADDR_IER);
    XVideo_proc_WriteReg(InstancePtr->Bus_a_BaseAddress, XVIDEO_PROC_BUS_A_ADDR_IER, Register | Mask);
}

void XVideo_proc_InterruptDisable(XVideo_proc *InstancePtr, u32 Mask) {
    u32 Register;

    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Register =  XVideo_proc_ReadReg(InstancePtr->Bus_a_BaseAddress, XVIDEO_PROC_BUS_A_ADDR_IER);
    XVideo_proc_WriteReg(InstancePtr->Bus_a_BaseAddress, XVIDEO_PROC_BUS_A_ADDR_IER, Register & (~Mask));
}

void XVideo_proc_InterruptClear(XVideo_proc *InstancePtr, u32 Mask) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XVideo_proc_WriteReg(InstancePtr->Bus_a_BaseAddress, XVIDEO_PROC_BUS_A_ADDR_ISR, Mask);
}

u32 XVideo_proc_InterruptGetEnabled(XVideo_proc *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return XVideo_proc_ReadReg(InstancePtr->Bus_a_BaseAddress, XVIDEO_PROC_BUS_A_ADDR_IER);
}

u32 XVideo_proc_InterruptGetStatus(XVideo_proc *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return XVideo_proc_ReadReg(InstancePtr->Bus_a_BaseAddress, XVIDEO_PROC_BUS_A_ADDR_ISR);
}

