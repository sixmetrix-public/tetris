// ==============================================================
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2019.2_AR73100 (64-bit)
// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// ==============================================================
#ifndef __linux__

#include "xstatus.h"
#include "xparameters.h"
#include "xvideo_proc.h"

extern XVideo_proc_Config XVideo_proc_ConfigTable[];

XVideo_proc_Config *XVideo_proc_LookupConfig(u16 DeviceId) {
	XVideo_proc_Config *ConfigPtr = NULL;

	int Index;

	for (Index = 0; Index < XPAR_XVIDEO_PROC_NUM_INSTANCES; Index++) {
		if (XVideo_proc_ConfigTable[Index].DeviceId == DeviceId) {
			ConfigPtr = &XVideo_proc_ConfigTable[Index];
			break;
		}
	}

	return ConfigPtr;
}

int XVideo_proc_Initialize(XVideo_proc *InstancePtr, u16 DeviceId) {
	XVideo_proc_Config *ConfigPtr;

	Xil_AssertNonvoid(InstancePtr != NULL);

	ConfigPtr = XVideo_proc_LookupConfig(DeviceId);
	if (ConfigPtr == NULL) {
		InstancePtr->IsReady = 0;
		return (XST_DEVICE_NOT_FOUND);
	}

	return XVideo_proc_CfgInitialize(InstancePtr, ConfigPtr);
}

#endif

