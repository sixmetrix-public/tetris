// ==============================================================
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2019.2_AR73100 (64-bit)
// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// ==============================================================
#ifndef XVIDEO_PROC_H
#define XVIDEO_PROC_H

#ifdef __cplusplus
extern "C" {
#endif

/***************************** Include Files *********************************/
#ifndef __linux__
#include "xil_types.h"
#include "xil_assert.h"
#include "xstatus.h"
#include "xil_io.h"
#else
#include <stdint.h>
#include <assert.h>
#include <dirent.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stddef.h>
#endif
#include "xvideo_proc_hw.h"

/**************************** Type Definitions ******************************/
#ifdef __linux__
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
#else
typedef struct {
    u16 DeviceId;
    u32 Bus_a_BaseAddress;
} XVideo_proc_Config;
#endif

typedef struct {
    u32 Bus_a_BaseAddress;
    u32 IsReady;
} XVideo_proc;

/***************** Macros (Inline Functions) Definitions *********************/
#ifndef __linux__
#define XVideo_proc_WriteReg(BaseAddress, RegOffset, Data) \
    Xil_Out32((BaseAddress) + (RegOffset), (u32)(Data))
#define XVideo_proc_ReadReg(BaseAddress, RegOffset) \
    Xil_In32((BaseAddress) + (RegOffset))
#else
#define XVideo_proc_WriteReg(BaseAddress, RegOffset, Data) \
    *(volatile u32*)((BaseAddress) + (RegOffset)) = (u32)(Data)
#define XVideo_proc_ReadReg(BaseAddress, RegOffset) \
    *(volatile u32*)((BaseAddress) + (RegOffset))

#define Xil_AssertVoid(expr)    assert(expr)
#define Xil_AssertNonvoid(expr) assert(expr)

#define XST_SUCCESS             0
#define XST_DEVICE_NOT_FOUND    2
#define XST_OPEN_DEVICE_FAILED  3
#define XIL_COMPONENT_IS_READY  1
#endif

/************************** Function Prototypes *****************************/
#ifndef __linux__
int XVideo_proc_Initialize(XVideo_proc *InstancePtr, u16 DeviceId);
XVideo_proc_Config* XVideo_proc_LookupConfig(u16 DeviceId);
int XVideo_proc_CfgInitialize(XVideo_proc *InstancePtr, XVideo_proc_Config *ConfigPtr);
#else
int XVideo_proc_Initialize(XVideo_proc *InstancePtr, const char* InstanceName);
int XVideo_proc_Release(XVideo_proc *InstancePtr);
#endif

void XVideo_proc_Start(XVideo_proc *InstancePtr);
u32 XVideo_proc_IsDone(XVideo_proc *InstancePtr);
u32 XVideo_proc_IsIdle(XVideo_proc *InstancePtr);
u32 XVideo_proc_IsReady(XVideo_proc *InstancePtr);
void XVideo_proc_EnableAutoRestart(XVideo_proc *InstancePtr);
void XVideo_proc_DisableAutoRestart(XVideo_proc *InstancePtr);


void XVideo_proc_InterruptGlobalEnable(XVideo_proc *InstancePtr);
void XVideo_proc_InterruptGlobalDisable(XVideo_proc *InstancePtr);
void XVideo_proc_InterruptEnable(XVideo_proc *InstancePtr, u32 Mask);
void XVideo_proc_InterruptDisable(XVideo_proc *InstancePtr, u32 Mask);
void XVideo_proc_InterruptClear(XVideo_proc *InstancePtr, u32 Mask);
u32 XVideo_proc_InterruptGetEnabled(XVideo_proc *InstancePtr);
u32 XVideo_proc_InterruptGetStatus(XVideo_proc *InstancePtr);

#ifdef __cplusplus
}
#endif

#endif
