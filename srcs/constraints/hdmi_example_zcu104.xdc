                              #####
## Constraints for ZCU104 FMC HDMI 2.0
## Version 1.0
#####


#####
## Pins
#####

# TBD
set_property PACKAGE_PIN M11 [get_ports reset]
set_property IOSTANDARD LVCMOS33 [get_ports reset]

# HDMI TX
set_property PACKAGE_PIN T8 [get_ports TX_REFCLK_P_IN]
create_clock -name tx_mgt_refclk -period 3.367 [get_ports TX_REFCLK_P_IN]

# I2C
set_property IOSTANDARD LVCMOS33 [get_ports fmch_iic_scl_io]
set_property PACKAGE_PIN D1 [get_ports fmch_iic_scl_io]
set_property IOSTANDARD LVCMOS33 [get_ports fmch_iic_sda_io]
set_property PACKAGE_PIN E1 [get_ports fmch_iic_sda_io]

# rev B
set_property PACKAGE_PIN G21 [get_ports {HDMI_TX_CLK_P_OUT}]
set_property IOSTANDARD LVDS [get_ports {HDMI_TX_CLK_P_OUT}]

# TBD
# set_property PACKAGE_PIN G8 [get_ports tmds_clk_out]
# set_property IOSTANDARD LVCMOS33 [get_ports tmds_clk_out]

set_property PACKAGE_PIN E3 [get_ports TX_HPD_IN]
set_property IOSTANDARD LVCMOS33 [get_ports TX_HPD_IN]

set_property PACKAGE_PIN B1 [get_ports TX_DDC_OUT_scl_io]
set_property IOSTANDARD LVCMOS33 [get_ports TX_DDC_OUT_scl_io]

set_property PACKAGE_PIN C1 [get_ports TX_DDC_OUT_sda_io]
set_property IOSTANDARD LVCMOS33 [get_ports TX_DDC_OUT_sda_io]


## Camera sensor config
set_property PACKAGE_PIN B9 [get_ports IIC_sensor_scl_io]
set_property PACKAGE_PIN B8 [get_ports IIC_sensor_sda_io]
set_property PULLUP true [get_ports IIC_sensor_scl_io]
set_property PULLUP true [get_ports IIC_sensor_sda_io]
#set_property IOSTANDARD HSUL_12_DCI [get_ports IIC_sensor_scl_io]
#set_property IOSTANDARD HSUL_12_DCI [get_ports IIC_sensor_sda_io]
set_property IOSTANDARD LVCMOS18 [get_ports IIC_sensor_*]

#
# The VRP pin in Bank 67 is not connected (NC). To use MIPI_DPHY_DCI on this bank,
# DCI Cascade must be used. Since Bank 66 has a 240 ohm resistor connected to the
# VRP pin, use Bank 66 as a DCI cascade master bank for Bank 66 (see AR# 67565).
#
set_property DCI_CASCADE {67} [get_iobanks 66]

# GPIO Configuration
set_property PACKAGE_PIN H12 [get_ports {GPIO_sensor_tri_o[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_sensor_tri_o[0]}]
set_property PACKAGE_PIN A8 [get_ports {GPIO_sensor_tri_o[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {GPIO_sensor_tri_o[1]}]
set_property PACKAGE_PIN C17 [get_ports {GPIO_sensor_tri_o[2]}]
set_property IOSTANDARD LVCMOS12 [get_ports {GPIO_sensor_tri_o[2]}]

#set_property IOSTANDARD LVCMOS12 [get_ports {GPIO_sensor_tri_o[*]}]

## Other

# Misc
#GPIO_LED_0_LS
set_property PACKAGE_PIN D5 [get_ports LED0]
set_property IOSTANDARD LVCMOS33 [get_ports {LED0}]
set_property PACKAGE_PIN N11 [get_ports IDT_8T49N241_LOL_IN]
set_property IOSTANDARD LVCMOS33 [get_ports IDT_8T49N241_LOL_IN]

set_property PACKAGE_PIN A2 [get_ports TX_EN_OUT]
set_property IOSTANDARD LVCMOS33 [get_ports TX_EN_OUT]

# TBD - not needed as it is duplicated of TX_EN_OUT
# set_property PACKAGE_PIN C2 [get_ports {RX_LS_OE[0]}]
# set_property IOSTANDARD LVCMOS33 [get_ports {RX_LS_OE[0]}]

#####
## End
#####

