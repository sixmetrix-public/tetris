#Build the project
#source D:/SixMetrixInternal/Tetris/scripts/set_revision.tcl

#Run synthesis
update_compile_order -fileset sources_1
launch_runs synth_1 -jobs 4

#Wait until synthesis complete
wait_on_run synth_1

#Run implementation
launch_runs impl_1 -jobs 4

#Wait until implementation complete
wait_on_run impl_1

#If need add encryption

#run bitstream generation
launch_runs impl_1 -to_step write_bitstream -jobs 4


#Export design to sdk
#file copy -force d:/Projects/Pepper/xilinx/Pepper.runs/impl_1/Pepper_top.sysdef D:/Projects/Pepper/sdk/Pepper_top.hdf

#Export report to pepper/report
#file copy -force d:/Projects/Pepper/xilinx/Pepper.runs/impl_1/Pepper_top_timing_summary_routed.rpt D:/Projects/Pepper/reports/Pepper_top_timing_summary_routed.rpt
#file copy -force d:/Projects/Pepper/xilinx/Pepper.runs/impl_1/Pepper_top_utilization_placed.rpt D:/Projects/Pepper/reports/Pepper_top_utilization_placed.rpt
#file copy -force d:/Projects/Pepper/xilinx/Pepper.runs/impl_1/Pepper_top_power_routed.rpt D:/Projects/Pepper/reports/Pepper_top_power_routed.rpt


