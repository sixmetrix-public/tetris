proc create_git_revision_entity {git_path entity_name dest_path_dir} {

#cd git location path
set root [pwd]
cd $git_path

set rev_id [exec git rev-parse --short HEAD]
set posix_time [exec git log -1 --pretty=format:%ct]
set DATE  [exec git log -1 --pretty=format:%ad --date=format:%d%m%Y]

set output_filename ""
append output_filename $dest_path_dir "revision_" $entity_name ".vhd"
set outfile [open $output_filename w+]

puts $outfile "--------------------------------------------"
puts $outfile "--$entity_name HEAD REVISION ID "
puts $outfile "--Obtain using the following GIT CMD"
puts $outfile "--git rev-parse --short HEAD"
puts $outfile "--------------------------------------------"
puts $outfile "\n"


puts $outfile "library IEEE;"
puts $outfile "use IEEE.STD_LOGIC_1164.ALL;"
puts $outfile "use IEEE.numeric_std.ALL;"
puts $outfile "entity REVISION_[string toupper $entity_name] is"
puts $outfile "Port ("
puts $outfile "	HASH_ID : OUT STD_LOGIC_VECTOR(31 downto 0);"
puts $outfile "	POSIXTIME : OUT STD_LOGIC_VECTOR(31 downto 0);"
puts $outfile "	DATE : OUT STD_LOGIC_VECTOR(31 downto 0)"
puts $outfile ");"
puts $outfile "end REVISION_[string toupper $entity_name];"
puts $outfile ""
puts $outfile "architecture beh of REVISION_[string toupper $entity_name] is"
puts $outfile ""
puts $outfile "constant c_hash : std_logic_vector(31 downto 0) := x\"0$rev_id\";"
puts $outfile "constant c_posix : std_logic_vector(31 downto 0) := std_logic_vector(to_signed($posix_time,32));"
puts $outfile "constant c_date : std_logic_vector(31 downto 0) := x\"$DATE\";"
puts $outfile ""
puts $outfile "begin"
puts $outfile ""
puts $outfile "HASH_ID <= c_hash;"
puts $outfile "POSIXTIME <= c_posix;"
puts $outfile "DATE <= c_date;"
puts $outfile ""
puts $outfile "end beh;"

close $outfile

cd $root
}